$(document).ready(function() {
 
    //datatables
    var base_url = $('#base_url').val();
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url+"order/instagram/get_data",
            "type": "GET"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
 
});

$('#type').on('change' , function(){
    var type = $('#type').val();
    // alert(type)
    if (type == 'comment') {
        html = '<div class="col-md-12"><div class="form-group"><label>Komentar <small>Gunakan tanda "|" untuk memisahkan Komentar </small></label><textarea name="comment" class="form-control" placeholder="Contk komentar 1 | contoh Komentar 2 |"></textarea></div></div>'
    }else{
        html = '<div class="col-md-12"><div class=""><label>Jumlah</label><input type="number" name="jumlah" class="form-control"></div></div>'
    }
    $('#addtional_form').html(html);
})
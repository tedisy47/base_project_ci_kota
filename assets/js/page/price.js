$(document).ready(function() {
 
    //datatables
    var base_url = $('#base_url').val();
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url+"master/price/get_data",
            "type": "GET"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
 
});

$('#platfrom').on('change', function() {
  alert( this.value );
  var option = ''
  if (this.value == 'ig') {
        option += '<option value="follow">Follow</option>';
        option += '<option value="like">Like</option>';
        option += '<option value="view">View</option>';
        option += '<option value="comment">Comment</option>';
    }else if(this.value == 'twitter' ){

        option += '<option value="follow">Follow</option>';
        option += '<option value="like">Like</option>';
        option += '<option value="comment">Comment</option>';
        option += '<option value="retweet">Retweet</option>';

    }else if (this.value == 'yt') {

        option += '<option value="follow">Follow</option>';
        option += '<option value="like">Like</option>';
        option += '<option value="comment">Comment</option>';
        option += '<option value="retweet">Retweet</option>';
    }

    $('#type').html(option)
});
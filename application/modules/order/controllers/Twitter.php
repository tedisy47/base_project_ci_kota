<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class twitter extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();

		if (!$this->session->userdata('user_id')) {
			# code...
			redirect();
		}


		$this->load->model('M_twitter','twitter');
		$this->load->model('M_twitter_follow','twitter_follow');
		$this->load->model('M_twitter_like','twitter_like');
		$this->load->model('M_twitter_comment','twitter_comment');
	}
	public function index()
	{
		$data['title'] = 'twitter';
		$data['field'] = $this->twitter->field();
		$data['js'] = 'assets/js/page/twitter.js';
		$this->load->view('template/table',$data);
	}
	public function get_data()
	{
		$list = $this->twitter->get_data();
        $data = array();
        $no = $_GET['start'];
        foreach ($list as $twitter) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $twitter->type;
            $row[] = $twitter->link;
            $row[] = $twitter->jumlah;
            if ($twitter->status=='proses') {
            	$row[] = '<i class="text-danger">'.$twitter->status.'</i>';
            }else{
            	$row[] = '<i class="text-success">'.$twitter->status.'</i>';
            }
            $row[] = '';
 
            $data[] = $row;
        }
        // echo "<pre>";
        // print_r($data);die;
 
        $output = array(
                        "draw" => $_GET['draw'],
                        "recordsTotal" => $this->twitter->count_all(),
                        "recordsFiltered" => $this->twitter->count_filter(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}
	public function form($id=0)
	{
		$option['type']  = array(
			array('label' =>'Follow' , 'value' => 'follow' ),
			array('label' =>'Like' , 'value' => 'like' ),
			array('label' =>'Comment' , 'value' => 'comment' ),
		);
		$data_twitter = array();
		if ($id!=0) {
			# code...
			$data_twitter = $this->twitter->edit($id);
		}
		$data['url_save'] = site_url('order/twitter/save');
		$data['title'] = 'twitter';
		$data['form'] = form_bulder($this->twitter->field(),$option,$data_twitter);
		$data['js'] = 'assets/js/page/twitter.js';
		$this->load->view('template/form',$data);
	}
	public function save()
	{
		$post = $this->input->post();
		// print_r($post);die;
		// https://www.twitter.com/
		$field = $this->twitter->field();
		$config = array();
		foreach ($field as $k => $v) {
			if ($v['form_show']=='Y' AND $v['name'] != 'id') {
				# code...
				$config[$k] =  array(
			                'field' => $v['name'],
			                'label' => $v['label'],
			                'rules' => 'required',
			                'errors' => array(
			                		'required' =>  $v['label'].' harus di isi',
			                ),
		                );
			}
			# code...
		}
		
		
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE)
        {
        	foreach ($field as $key => $value) {
        		if ($v['form_show']=='Y' AND $value['name'] != 'id') {

        			$this->session->set_flashdata('error_'.$value['name'],form_error($value['name']));
        			$this->session->set_flashdata('data_'.$value['name'],set_value($value['name']));
        		}
        		# code...
        	}
        	if ($post['id']==0) {
        		redirect('order/twitter/form');
        	}else{
        		redirect('order/twitter/form'.$post['id']);
        	}
        }
        else
        {
        	foreach ($field as $key => $value) {
        		if($value['form_show']=='Y' AND $value['name'] != 'id'){
        			$data[$value['name']] = $post[$value['name']];
        		}
        	}
        	$username_ig = str_replace('https://www.twitter.com/', '', $post['link']);
        	$username_ig = str_replace('/', '', $username_ig); 
        	echo "<pre>";
        	print_r($data);
            die;
            if ($post['type'] == 'comment') {
                $count_comment = explode('|', $post['comment']);
                $count_comment = count($count_comment);
                $data['jumlah'] = $count_comment;
            }else{

                $data['jumlah'] = $post['jumlah'];

            }
        	$data['user_id'] =  $this->session->userdata('user_id');
    		$order_id = $this->twitter->insert($data);

        	if ($post['type'] == 'comment') {
        		$count_comment = explode('|', $post['comment']);
        		$count_comment = count($count_comment);

        		$data_comment['url'] = $post['link'];
        		$data_comment['comment'] = $post['comment']; 
        		$data_comment['status'] = 0;
        		$this->twitter_comment->insert($data_comment);
        	}elseif ($post['type'] == 'like') {
        		# code...
        		$data_like['url'] = $post['link'];
        		$data_like['jumlah'] = $data['jumlah']; 
        		$data_like['status'] = 0;
        		$this->twitter_like->insert($data_like);

        	}else{
        		$data_folow['username_following'] = $username_ig;
        		$data_folow['jumlah'] = $data['jumlah']; 
        		$data_folow['status'] = 0;
                $data_folow['order_id'] = $order_id;
        		$this->twitter_follow->insert($data_folow);

        	}
    		$alert_message = 'Order Berhasil Di proses';
        	// print_r($data);die;
        	
			$this->session->set_flashdata('alert','<script>swal("Berhasil", "'.$alert_message.'", "success");</script>');
        	redirect('order/twitter');
        }
	}
}
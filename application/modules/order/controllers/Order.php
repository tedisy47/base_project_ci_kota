<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class order extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_order','order');
		$this->load->model('M_order_detail','order_detail');
	}
	public function index()
	{
		$data['title'] = 'order';
		$data['field'] = $this->order->field();
		$data['js'] = 'assets/js/page/order.js';
		$this->load->view('template/table',$data);
	}
	public function get_data()
	{
		$list = $this->order->get_data();
        $data = array();
        $no = $_GET['start'];
        foreach ($list as $order) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $order->name;
            $row[] = $order->email;
            $row[] = $order->subject;
            if ($order->status=='close') {
            	$row[] = '<i class="text-danger">'.$order->status.'</i>';
            }else if ($order->status == 'open') {
            	$row[] = '<i class="text-primary">'.$order->status.'</i>';
            }else{
            	$row[] = '<i class="text-success">'.$order->status.'</i>';
            }
            $row[] = '<a href="'.site_url('master/order/detail/'.$order->id).'" class="btn btn-xs btn-warning"><i class="fa fa-info"></i></a>&nbsp;&nbsp;';
 
            $data[] = $row;
        }
        // echo "<pre>";
        // print_r($data);die;
 
        $output = array(
                        "draw" => $_GET['draw'],
                        "recordsTotal" => $this->order->count_all(),
                        "recordsFiltered" => $this->order->count_filter(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}
	public function detail($id = 0)
	{
		$data['title'] = 'order';
		$data['order'] = $this->order->detail($id);
		$data['order_detail'] = $this->order_detail->detail($id);
		$data['order_id'] = $id;
		$data['js'] = 'assets/js/page/order.js';
		// echo "<pre>";
		// print_r($data);die;
		$this->load->view('template/order',$data);

	}
	public function submit_comment()
	{

		$post = $this->input->post();
		$data['order_id'] = $post['order_id'];
		$data['comment'] = $post['comment'];
		$data['order_id'] = $post['order_id'];
		$data['order_id'] = $post['order_id']; 
		$data['user_id'] = $this->session->userdata('user_id'); 
		$this->order_detail->insert($data);
		redirect('master/order/detail/'.$data['order_id']);
	}
	
}
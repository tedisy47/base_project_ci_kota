<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class instagram extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();

		if (!$this->session->userdata('user_id')) {
			# code...
			redirect();
		}


        $this->load->model('M_instagram','instagram');
        $this->load->model('master/M_price','price');
		$this->load->model('M_instagram_follow','instagram_follow');
		$this->load->model('M_instagram_like','instagram_like');
		$this->load->model('M_instagram_comment','instagram_comment');
	}
	public function index()
	{
		$data['title'] = 'instagram';
		$data['field'] = $this->instagram->field();
		$data['js'] = 'assets/js/page/instagram.js';
		$this->load->view('template/table',$data);
	}
	public function get_data()
	{
		$list = $this->instagram->get_data();
        $data = array();
        $no = $_GET['start'];
        foreach ($list as $instagram) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $instagram->type;
            $row[] = $instagram->link;
            $row[] = $instagram->jumlah;
            if ($instagram->status=='proses') {
            	$row[] = '<i class="text-danger">'.$instagram->status.'</i>';
            }else{
            	$row[] = '<i class="text-success">'.$instagram->status.'</i>';
            }
            $row[] = '';
 
            $data[] = $row;
        }
        // echo "<pre>";
        // print_r($data);die;
 
        $output = array(
                        "draw" => $_GET['draw'],
                        "recordsTotal" => $this->instagram->count_all(),
                        "recordsFiltered" => $this->instagram->count_filter(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}
	public function form($id=0)
	{
		$option['type']  = $this->price->get_option();
        // print_r($option);die;
		$data_instagram = array();
		if ($id!=0) {
			# code...
			$data_instagram = $this->instagram->edit($id);
		}
		$data['url_save'] = site_url('order/instagram/save');
		$data['title'] = 'instagram';
		$data['form'] = form_bulder($this->instagram->field(),$option,$data_instagram);
		$data['js'] = 'assets/js/page/instagram.js';
		$this->load->view('template/form',$data);
	}
	public function save()
	{
		$post = $this->input->post();
		// print_r($post);die;
		// https://www.instagram.com/
		$field = $this->instagram->field();
		$config = array();
		foreach ($field as $k => $v) {
			if ($v['form_show']=='Y' AND $v['name'] != 'id') {
				# code...
				$config[$k] =  array(
			                'field' => $v['name'],
			                'label' => $v['label'],
			                'rules' => 'required',
			                'errors' => array(
			                		'required' =>  $v['label'].' harus di isi',
			                ),
		                );
			}
			# code...
		}
		
		
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE)
        {
        	foreach ($field as $key => $value) {
        		if ($v['form_show']=='Y' AND $value['name'] != 'id') {

        			$this->session->set_flashdata('error_'.$value['name'],form_error($value['name']));
        			$this->session->set_flashdata('data_'.$value['name'],set_value($value['name']));
        		}
        		# code...
        	}
        	if ($post['id']==0) {
        		redirect('order/instagram/form');
        	}else{
        		redirect('order/instagram/form'.$post['id']);
        	}
        }
        else
        {
        	foreach ($field as $key => $value) {
        		if($value['form_show']=='Y' AND $value['name'] != 'id'){
        			$data[$value['name']] = $post[$value['name']];
        		}
        	}
        	$username_ig = str_replace('https://www.instagram.com/', '', $post['link']);
        	$username_ig = str_replace('/', '', $username_ig); 
        	// echo "<pre>";
        	// print_r($post);
        	$data['user_id'] =  $this->session->userdata('user_id');

    		$order_id = $this->instagram->insert($data);

        	if ($post['type'] == 'comment') {
        		$count_comment = explode('|', $post['comment']);
        		$count_comment = count($count_comment);
        		$data['jumlah'] = $count_comment;

        		$data_comment['url'] = $post['link'];
        		$data_comment['comment'] = $post['comment']; 
        		$data_comment['status'] = 0;
        		$this->instagram_comment->insert($data_comment);
        	}elseif ($post['type'] == 'like') {
        		# code...
        		$data['jumlah'] = $post['jumlah'];
        		$data_like['url'] = $post['link'];
        		$data_like['jumlah'] = $data['jumlah']; 
        		$data_like['status'] = 0;
        		$this->instagram_like->insert($data_like);

        	}else{
        		$data['jumlah'] = $post['jumlah'];
        		$data_folow['username_following'] = $username_ig;
        		$data_folow['jumlah'] = $data['jumlah']; 
        		$data_folow['status'] = 0;
                $data_folow['order_id'] = $order_id;
        		$this->instagram_follow->insert($data_folow);

        	}
    		$alert_message = 'Order Berhasil Di proses';
        	// print_r($data);die;
        	
			$this->session->set_flashdata('alert','<script>swal("Berhasil", "'.$alert_message.'", "success");</script>');
        	redirect('order/instagram');
        }
	}
}
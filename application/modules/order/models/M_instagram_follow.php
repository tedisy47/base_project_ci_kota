<?php
defined('BASEPATH') OR exit('no direct script access allowed');

class M_instagram_follow extends MY_Model {
	
	public $table;
	function __construct()
	{
		// nama database
		parent::__construct();
		$this->table['name'] = 'ig_following';
		// id primary
		$this->table['primary_key'] = 'id';
		// field
		$this->table['field']  = array(
			array('name' => 'id','type'=>'HIDDEN','label'=>'id','table_show'=>'N','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id2' ),
			array('name' => 'type','type'=>'select','label'=>'Type','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'type' ),
			array('name' => 'link','type'=>'text','label'=>'link','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id' ),
		);
	}
	public function get_data()
	{
		$params['select'] = '*';
		$field = $this->table['field'];
		return $this->get_datatable($field,$params);
	}
	public function count_filter()
	{
		$params['select'] = 'id';
		return $this->count_filtered($params);
	}

}
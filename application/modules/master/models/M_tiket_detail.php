<?php
defined('BASEPATH') OR exit('no direct script access allowed');

class M_tiket_detail extends MY_Model {
	
	public $table;
	function __construct()
	{
		// nama database
		parent::__construct();
		$this->table['name'] = 'tiket_detail';
		// id primary
		$this->table['primary_key'] = 'id';
		// field
		$this->table['field']  = array(
			array('name' => 'id','type'=>'HIDDEN','label'=>'Foto Profil','table_show'=>'N','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id2' ),
			array('name' => 'user_id','type'=>'text','label'=>'Nama','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id' ),
			array('name' => 'email','type'=>'text','label'=>'Email','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id' ),
			array('name' => 'subject','type'=>'text','label'=>'Subject','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id' ),
			array('name' => 'status','type'=>'text','label'=>'Status','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id' ),
		);
	}
	public function get_data()
	{
		$params['select'] = 'tiket_detail.*, user.name, user.email';
		$params['join']  = array(
			array('table' => 'user','id'=>'user.id','as'=>'tiket_detail.user_id','join'=>'left'),
		);
		$field = $this->table['field'];
		return $this->get_datatable($field,$params);
	}
	public function count_filter()
	{
		$params['select'] = 'id';
		return $this->count_filtered($params);
	}
	public function detail($id)
	{
		$params['select'] = 'tiket_detail.*, user.name, user.email';
		$params['join']  = array(
			array('table' => 'user','id'=>'user.id','as'=>'tiket_detail.user_id','join'=>'left'),
		);
		$params['where'] = 'tiket_id = '.$id;
		$params['order_by']  = array('name' => 'tiket_detail.id','value' => 'ASC' );
		return $this->get($params)->result_array();	
	}

}
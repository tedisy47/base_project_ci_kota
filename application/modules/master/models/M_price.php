<?php
defined('BASEPATH') OR exit('no direct script access allowed');

class M_price extends MY_Model {
	
	public $table;
	function __construct()
	{
		// nama database
		parent::__construct();
		$this->table['name'] = 'price';
		// id primary
		$this->table['primary_key'] = 'id';
		// field
		$this->table['field']  = array(
			array('name' => 'id','type'=>'HIDDEN','label'=>'Foto Profil','table_show'=>'N','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id2' ),
			array('name' => 'Price_name','type'=>'text','label'=>'name','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id' ),
			array('name' => 'platfrom','type'=>'select','label'=>'Platform','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'platfrom' ),
			array('name' => 'type','type'=>'select','label'=>'type','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'type' ),
			array('name' => 'Price_description','type'=>'TEXTAREA','label'=>'Deskripsi','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id' ),
			array('name' => 'Price_harga','type'=>'NUMBER','label'=>'Harga','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id' )
		);
	}
	public function get_data()
	{
		$params['select'] = '*';
		$field = $this->table['field'];
		return $this->get_datatable($field,$params);
	}
	public function count_filter()
	{
		$params['select'] = 'id';
		return $this->count_filtered($params);
	}
	public function get_option()
	{
		$params['select'] = '*';
		$result =  $this->get($params)->result_array();
		foreach ($result as $key => $value) {
			# code...
			if ($value['platfrom'] == 'ig') {
				# code...
				$value['platfrom'] = 'Instagram';
			}elseif ($value['platfrom'] == 'yt') {
				# code...
				$value['platfrom'] = 'Youtube';
			}
			$data[$key]['label'] = $value['platfrom'].'-'.$value['type'].'-'.$value['price_harga'].'-'.$value['price_description'];
			$data[$key]['value'] = $value['id'];
		}
		return $data;
		// print_r($data);die;
		
	}

}
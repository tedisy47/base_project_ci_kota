<?php
defined('BASEPATH') OR exit('no direct script access allowed');

class M_account_ig extends MY_Model {
	
	public $table;
	function __construct()
	{
		// nama database
		parent::__construct();
		$this->table['name'] = 'ig_followers';
		// id primary
		$this->table['primary_key'] = 'id';
		// field
		$this->table['field']  = array(
			array('name' => 'id','type'=>'HIDDEN','label'=>'Foto Profil','table_show'=>'N','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id2' ),
			array('name' => 'username','type'=>'text','label'=>'Username','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id' ),
			array('name' => 'password','type'=>'text','label'=>'Password','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id' ),
		);
	}
	public function get_data()
	{
		$params['select'] = '*';
		$field = $this->table['field'];
		return $this->get_datatable($field,$params);
	}
	public function count_filter()
	{
		$params['select'] = 'id';
		return $this->count_filtered($params);
	}

}
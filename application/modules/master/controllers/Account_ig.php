<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class account_ig extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('user_id')) {
			# code...
			redirect();
		}
		$this->load->model('M_account_ig','account_ig');
	}
	public function index()
	{
		$data['title'] = 'account_ig';
		$data['url_add'] = site_url('master/account_ig/form');
		$data['field'] = $this->account_ig->field();
		$data['js'] = 'assets/js/page/account_ig.js';
		$this->load->view('template/table',$data);
	}
	public function get_data()
	{
		$list = $this->account_ig->get_data();
        $data = array();
        $no = $_GET['start'];
        foreach ($list as $account_ig) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $account_ig->username;
            $row[] = $account_ig->password;
            $row[] = '<a href="'.site_url('master/account_ig/form/'.$account_ig->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;'.
            		'<a href="'.site_url('master/account_ig/delete/'.$account_ig->id).'" onclick="return confirm(`apakah ingin menghapus kategori ini`)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>&nbsp;&nbsp;';
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_GET['draw'],
                        "recordsTotal" => $this->account_ig->count_all(),
                        "recordsFiltered" => $this->account_ig->count_filter(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}
	public function form($id=0)
	{
		$data_account_ig = array();
		if ($id!=0) {
			# code...
			$data_account_ig = $this->account_ig->edit($id);
		}
		$data['url_save'] = site_url('master/account_ig/save');
		$data['title'] = 'account_ig';
		$data['form'] = form_bulder($this->account_ig->field(),array(),$data_account_ig);
		$data['js'] = 'assets/js/page/account_ig.js';
		$this->load->view('template/form',$data);
	}
	public function save()
	{
		$post = $this->input->post();

		$field = $this->account_ig->field();
		$config = array();
		foreach ($field as $k => $v) {
			if ($v['form_show']=='Y' AND $v['name'] != 'id') {
				# code...
				$config[$k] =  array(
			                'field' => $v['name'],
			                'label' => $v['label'],
			                'rules' => 'required',
			                'errors' => array(
			                		'required' =>  $v['label'].' harus di isi',
			                ),
		                );
			}
			# code...
		}
		
		
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE)
        {
        	foreach ($field as $key => $value) {
        		if ($v['form_show']=='Y' AND $value['name'] != 'id') {

        			$this->session->set_flashdata('error_'.$value['name'],form_error($value['name']));
        			$this->session->set_flashdata('data_'.$value['name'],set_value($value['name']));
        		}
        		# code...
        	}
        	if ($post['id']==0) {
        		redirect('master/account_ig/form');
        	}else{
        		redirect('master/account_ig/form'.$post['id']);
        	}
        }
        else
        {
        	foreach ($field as $key => $value) {
        		if($value['form_show']=='Y' AND $value['name'] != 'id'){
        			$data[$value['name']] = $post[$value['name']];
        		}
        	}
        	if ($post['id']==0) {
        		# code...
        		$this->account_ig->insert($data);
        	}else{
        		$this->account_ig->update($post['id'],$data);
        	}
        	if ($post['id']=0) {
        		# code...
        		$alert_message = 'kategori Berhasil Ditambah';
        	}else{

        		$alert_message = 'kategori Berhasil Dirubah';
        	}
			$this->session->set_flashdata('alert','<script>swal("Berhasil", "'.$alert_message.'", "success");</script>');
        	redirect('master/account_ig');
        }
	}
	public function delete($id=0)
	{
		$this->account_ig->delete($id);
		$this->session->set_flashdata('alert','<script>swal("Berhasil", "Kategori Berhasil Di Hapus", "success");</script>');
		redirect('master/account_ig');
	}
}
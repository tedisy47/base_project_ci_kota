<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tiket extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('user_id')) {
			# code...
			redirect();
		}
		$this->load->model('M_tiket','tiket');
		$this->load->model('M_tiket_detail','tiket_detail');
	}
	public function index()
	{
		$data['title'] = 'tiket';
		$data['field'] = $this->tiket->field();
		$data['js'] = 'assets/js/page/tiket.js';
		$this->load->view('template/table',$data);
	}
	public function get_data()
	{
		$list = $this->tiket->get_data();
        $data = array();
        $no = $_GET['start'];
        foreach ($list as $tiket) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $tiket->name;
            $row[] = $tiket->email;
            $row[] = $tiket->subject;
            if ($tiket->status=='close') {
            	$row[] = '<i class="text-danger">'.$tiket->status.'</i>';
            }else if ($tiket->status == 'open') {
            	$row[] = '<i class="text-primary">'.$tiket->status.'</i>';
            }else{
            	$row[] = '<i class="text-success">'.$tiket->status.'</i>';
            }
            $row[] = '<a href="'.site_url('master/tiket/detail/'.$tiket->id).'" class="btn btn-xs btn-warning"><i class="fa fa-info"></i></a>&nbsp;&nbsp;';
 
            $data[] = $row;
        }
        // echo "<pre>";
        // print_r($data);die;
 
        $output = array(
                        "draw" => $_GET['draw'],
                        "recordsTotal" => $this->tiket->count_all(),
                        "recordsFiltered" => $this->tiket->count_filter(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}
	public function detail($id = 0)
	{
		$data['title'] = 'tiket';
		$data['tiket'] = $this->tiket->detail($id);
		$data['tiket_detail'] = $this->tiket_detail->detail($id);
		$data['tiket_id'] = $id;
		$data['js'] = 'assets/js/page/tiket.js';
		// echo "<pre>";
		// print_r($data);die;
		$this->load->view('template/tiket',$data);

	}
	public function submit_comment()
	{

		$post = $this->input->post();
		$data['tiket_id'] = $post['tiket_id'];
		$data['comment'] = $post['comment'];
		$data['tiket_id'] = $post['tiket_id'];
		$data['tiket_id'] = $post['tiket_id']; 
		$data['user_id'] = $this->session->userdata('user_id'); 
		$this->tiket_detail->insert($data);
		redirect('master/tiket/detail/'.$data['tiket_id']);
	}
	
}
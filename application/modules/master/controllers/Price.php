<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class price extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('user_id')) {
			# code...
			redirect();
		}
		$this->load->model('M_price','price');
	}
	public function index()
	{
		$data['title'] = 'price';
		$data['url_add'] = site_url('master/price/form');
		$data['field'] = $this->price->field();
		$data['js'] = 'assets/js/page/price.js';
		$this->load->view('template/table',$data);
	}
	public function get_data()
	{
		$list = $this->price->get_data();
        $data = array();
        $no = $_GET['start'];
        foreach ($list as $price) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $price->price_name;
            $row[] = $price->platfrom;
            $row[] = $price->type;
            $row[] = $price->price_description;
            $row[] = 'Rp '.number_format($price->price_harga);
            $row[] = '<a href="'.site_url('master/price/form/'.$price->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;'.
            		'<a href="'.site_url('master/price/delete/'.$price->id).'" onclick="return confirm(`apakah ingin menghapus price ini`)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>&nbsp;&nbsp;';
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_GET['draw'],
                        "recordsTotal" => $this->price->count_all(),
                        "recordsFiltered" => $this->price->count_filter(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}
	public function form($id=0)
	{
		$option['platfrom']  = array(
			array('label' => 'Instagram' , 'value' => 'ig' ),
			array('label' => 'Twitter' , 'value' => 'twitter' ),
			array('label' => 'Youtube' , 'value' => 'ty' ),
		); 
		$option['type']  = array(); 
		$data_price = array();
		if ($id!=0) {
			# code...
			$data_price = $this->price->edit($id);
			// print_r($data_price);die;
		}
		$data['url_save'] = site_url('master/price/save');
		$data['title'] = 'price';
		$data['form'] = form_bulder($this->price->field(),$option,$data_price);
		$data['js'] = 'assets/js/page/price.js';
		$this->load->view('template/form',$data);
	}
	public function save()
	{
		$post = $this->input->post();
		// print_r($post);die;
		$field = $this->price->field();
		$config = array();
		foreach ($field as $k => $v) {
			if ($v['form_show']=='Y' AND $v['name'] != 'id') {
				# code...
				$config[$k] =  array(
			                'field' => $v['name'],
			                'label' => $v['label'],
			                'rules' => 'required',
			                'errors' => array(
			                		'required' =>  $v['label'].' harus di isi',
			                ),
		                );
			}
			# code...
		}
		
		
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE)
        {
        	foreach ($field as $key => $value) {
        		if ($v['form_show']=='Y' AND $value['name'] != 'id') {

        			$this->session->set_flashdata('error_'.$value['name'],form_error($value['name']));
        			$this->session->set_flashdata('data_'.$value['name'],set_value($value['name']));
        		}
        		# code...
        	}
        	if ($post['id']==0) {
        		redirect('master/price/form');
        	}else{
        		redirect('master/price/form'.$post['id']);
        	}
        }
        else
        {
        	foreach ($field as $key => $value) {
        		if($value['form_show']=='Y' AND $value['name'] != 'id'){
        			$data[$value['name']] = $post[$value['name']];
        		}
        	}
        	if ($post['id']==0) {
        		# code...
        		$this->price->insert($data);
        	}else{
        		$this->price->update($post['id'],$data);
        	}
        	if ($post['id']=0) {
        		# code...
        		$alert_message = 'price Berhasil Ditambah';
        	}else{

        		$alert_message = 'price Berhasil Dirubah';
        	}
			$this->session->set_flashdata('alert','<script>swal("Berhasil", "'.$alert_message.'", "success");</script>');
        	redirect('master/price');
        }
	}
	public function delete($id=0)
	{
		$this->price->delete($id);
		$this->session->set_flashdata('alert','<script>swal("Berhasil", "price Berhasil Di Hapus", "success");</script>');
		redirect('master/price');
	}
}
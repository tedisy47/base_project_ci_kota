<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class account_twitter extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('user_id')) {
			# code...
			redirect();
		}
		$this->load->model('M_account_twitter','account_twitter');
	}
	public function index()
	{
		$data['title'] = 'account_twitter';
		$data['url_add'] = site_url('master/account_twitter/form');
		$data['field'] = $this->account_twitter->field();
		$data['js'] = 'assets/js/page/account_twitter.js';
		$this->load->view('template/table',$data);
	}
	public function get_data()
	{
		$list = $this->account_twitter->get_data();
        $data = array();
        $no = $_GET['start'];
        foreach ($list as $account_twitter) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $account_twitter->username;
            $row[] = $account_twitter->password;
            $row[] = '<a href="'.site_url('master/account_twitter/form/'.$account_twitter->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;'.
            		'<a href="'.site_url('master/account_twitter/delete/'.$account_twitter->id).'" onclick="return confirm(`apakah ingin menghapus kategori ini`)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>&nbsp;&nbsp;';
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_GET['draw'],
                        "recordsTotal" => $this->account_twitter->count_all(),
                        "recordsFiltered" => $this->account_twitter->count_filter(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}
	public function form($id=0)
	{
		$data_account_twitter = array();
		if ($id!=0) {
			# code...
			$data_account_twitter = $this->account_twitter->edit($id);
		}
		$data['url_save'] = site_url('master/account_twitter/save');
		$data['title'] = 'account_twitter';
		$data['form'] = form_bulder($this->account_twitter->field(),array(),$data_account_twitter);
		$data['js'] = 'assets/js/page/account_twitter.js';
		$this->load->view('template/form',$data);
	}
	public function save()
	{
		$post = $this->input->post();

		$field = $this->account_twitter->field();
		$config = array();
		foreach ($field as $k => $v) {
			if ($v['form_show']=='Y' AND $v['name'] != 'id') {
				# code...
				$config[$k] =  array(
			                'field' => $v['name'],
			                'label' => $v['label'],
			                'rules' => 'required',
			                'errors' => array(
			                		'required' =>  $v['label'].' harus di isi',
			                ),
		                );
			}
			# code...
		}
		
		
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE)
        {
        	foreach ($field as $key => $value) {
        		if ($v['form_show']=='Y' AND $value['name'] != 'id') {

        			$this->session->set_flashdata('error_'.$value['name'],form_error($value['name']));
        			$this->session->set_flashdata('data_'.$value['name'],set_value($value['name']));
        		}
        		# code...
        	}
        	if ($post['id']==0) {
        		redirect('master/account_twitter/form');
        	}else{
        		redirect('master/account_twitter/form'.$post['id']);
        	}
        }
        else
        {
        	foreach ($field as $key => $value) {
        		if($value['form_show']=='Y' AND $value['name'] != 'id'){
        			$data[$value['name']] = $post[$value['name']];
        		}
        	}
        	if ($post['id']==0) {
        		# code...
        		$this->account_twitter->insert($data);
        	}else{
        		$this->account_twitter->update($post['id'],$data);
        	}
        	if ($post['id']=0) {
        		# code...
        		$alert_message = 'kategori Berhasil Ditambah';
        	}else{

        		$alert_message = 'kategori Berhasil Dirubah';
        	}
			$this->session->set_flashdata('alert','<script>swal("Berhasil", "'.$alert_message.'", "success");</script>');
        	redirect('master/account_twitter');
        }
	}
	public function delete($id=0)
	{
		$this->account_twitter->delete($id);
		$this->session->set_flashdata('alert','<script>swal("Berhasil", "Kategori Berhasil Di Hapus", "success");</script>');
		redirect('master/account_twitter');
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_user','user');
	}
	public function login_view()
	{
		$this->load->view('login');
	}
	public function login()
	{
		$post = $this->input->post();
		$check_user = $this->user->login($post['email'],md5($post['password']));
		if ($check_user->num_rows() > 0) {
			# code...
			$user  = $check_user->row_array();
			$this->session->set_userdata('user_id',$user['id']);
			$this->session->set_userdata('email',$user['email']);
			$this->session->set_userdata('name',$user['name']);
			$this->session->set_userdata('hak_akses',$user['hak_akses']);
			redirect('home');
		}
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect();
		
	}
	public function get_data()
	{
		$list = $this->user->get_data();
        $data = array();
        $no = $_GET['start'];
        foreach ($list as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '<img src="'.$customers->gambar.'" width="100px">';
            $row[] = $customers->FirstName;
            $row[] = $customers->LastName;
            $row[] = $customers->phone;
            $row[] = $customers->address;
            $row[] = $customers->city;
            $row[] = $customers->country;
            $row[] = $customers->testing;
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_GET['draw'],
                        "recordsTotal" => $this->user->count_all(),
                        "recordsFiltered" => $this->user->count_filter(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}
	public function form()
	{
		$option['test'] = $this->test->get_option();
		$option['country'] = array(
			array('value' =>  'indonesia','label'=>'ID'),
			array('value' =>  'Amerika','label'=>'USA')
		);
		$data['url_save'] = site_url('user/user/save');
		$data['title'] = 'test Form';
		$data['form'] = form_bulder($this->user->field(),$option);
		$data['js'] = 'assets/js/user.js';
		$this->load->view('template/form',$data);
	}
	public function save()
	{
		$config = array(
		        array(
		                'field' => 'FirstName',
		                'label' => 'nama depan',
		                'rules' => 'required|min_length[5]',
		                'errors' => array(
		                		'required' => 'nama harus di isi',
		                		'min_length' => 'minimal 5 karakter'
		                )
		        ),
		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE)
        {
        		// echo set_value('FirstName');die();
        	$data = array(
        		'error_FirstName' => form_error('FirstName'),
        		'data_FirstName' => set_value('FirstName'),
        	);
            $this->session->set_flashdata($data);
            redirect('user/user/form');
        }
        else
        {
                $this->load->view('formsuccess');
        }
	}
}
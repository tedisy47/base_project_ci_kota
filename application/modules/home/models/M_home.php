<?php
defined('BASEPATH') OR exit('no direct script access allowed');

class M_home extends MY_Model {
	
	public $table;
	function __construct()
	{
		// nama database
		parent::__construct();
		$this->table['name'] = 'customers';
		// id primary
		$this->table['id'] = 'id';
		// field
		$this->table['field']  = array(
			array('name' => 'id','type'=>'HIDDEN','label'=>'Foto Profil','table_show'=>'N','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id2' ),
			array('name' => 'gambar','type'=>'FILE','label'=>'Foto Profil','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id2' ),
			array('name' => 'FirstName','type'=>'text','label'=>'Nama Depan','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id' ),
			array('name' => 'LastName','type'=>'text','label'=>'Nama belakan','table_show'=>'Y','form_show'=>'N','col'=>'col-md-12','class'=>'form-control','id'=>'LastName' ),
			array('name' => 'phone','type'=>'NUMBER','label'=>'HP','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id2' ),
			array('name' => 'address','type'=>'TEXTAREA','label'=>'alamat','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id2' ),
			array('name' => 'city','type'=>'text','label'=>'Kota','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id2' ),
			array('name' => 'country','type'=>'RADIO','label'=>'Negara','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id2' ),
			array('name' => 'test','type'=>'select','label'=>'test','table_show'=>'Y','form_show'=>'Y','col'=>'col-md-12','class'=>'form-control','id'=>'id2' ),
		);
	}
	public function get_data()
	{
		$params['select'] = '*';
		$params['join']  = array(
			array('table' => 'test','id'=>'test.customer_id','as'=>'customers.id','join'=>'left'),
		);
		$field = $this->table['field'];
		return $this->get_datatable($field,$params);
	}
	public function count_filter()
	{
		$params['select'] = 'id';
		return $this->count_filtered($params);
	}

}
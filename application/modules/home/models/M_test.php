<?php
defined('BASEPATH') OR exit('no direct script access allowed');

class M_test extends MY_Model {
	
	public $table;
	function __construct()
	{
		// nama database
		parent::__construct();
		$this->table['name'] = 'test';
		// id primary
		$this->table['id'] = 'id';
		// field
		$this->table['field']  = array(
			array('name' => 'test','type'=>'text','label'=>'test','table_show'=>'Y','form_show'=>'Y','class'=>'col-md-12','id'=>'id2' ),
		);
	}
	public function get_option()
	{
		$params['select'] = '*';
		// $params['start'] = 0;
		// $params['limit'] = 1;
		$data =  $this->get($params);
		$options = array();
		foreach ($data as $key => $value) {
			$options[$key]['value'] = $value['customer_id'];
			$options[$key]['label'] = $value['testing'];
		}
		return $options;
	}
	public function count_filter()
	{
		$params['select'] = 'id';
		return $this->count_filtered($params);
	}

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_home','home');
		$this->load->model('M_test','test');
	}
	public function index()
	{
		$data['title'] = 'test';
		$data['field'] = $this->home->field();
		$data['js'] = 'assets/js/home.js';
		$this->load->view('template/dashboard',$data);
	}
	public function get_data()
	{
		$list = $this->home->get_data();
        $data = array();
        $no = $_GET['start'];
        foreach ($list as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->category_name;
            $row[] = $customers->LastName;
            $row[] = $customers->phone;
            $row[] = $customers->address;
            $row[] = $customers->city;
            $row[] = $customers->country;
            $row[] = $customers->testing;
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_GET['draw'],
                        "recordsTotal" => $this->home->count_all(),
                        "recordsFiltered" => $this->home->count_filter(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}
	public function form()
	{
		$option['test'] = $this->test->get_option();
		$option['country'] = array(
			array('value' =>  'indonesia','label'=>'ID'),
			array('value' =>  'Amerika','label'=>'USA')
		);
		$data['url_save'] = site_url('home/home/save');
		$data['title'] = 'test Form';
		$data['form'] = form_bulder($this->home->field(),$option);
		$data['js'] = 'assets/js/home.js';
		$this->load->view('template/form',$data);
	}
	public function save()
	{
		$config = array(
		        array(
		                'field' => 'FirstName',
		                'label' => 'nama depan',
		                'rules' => 'required|min_length[5]',
		                'errors' => array(
		                		'required' => 'nama harus di isi',
		                		'min_length' => 'minimal 5 karakter'
		                )
		        ),
		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE)
        {
        		// echo set_value('FirstName');die();
        	$data = array(
        		'error_FirstName' => form_error('FirstName'),
        		'data_FirstName' => set_value('FirstName'),
        	);
            $this->session->set_flashdata($data);
            redirect('home/home/form');
        }
        else
        {
                $this->load->view('formsuccess');
        }
	}
}
<?php (defined('BASEPATH')) OR exit('No direct script access allowed');


class MY_Model extends CI_model {

	 public function field()
	 {
	 	return $this->table['field'];
	 }
     public function get($params=array())
     {
        $this->db->select($params['select']);
        $this->db->from($this->table['name']);
        if (isset($params['join'])) {
            # code...
            foreach ($params['join'] as $key => $value) {
                # code...
                // print_r($params['join']);die;
                $this->db->join($value['table'],$value['id'].'='.$value['as'],$value['join']);
            }
        }
        if (isset($params['limit'])) {
            if (isset($params['start'])) {
                $this->db->limit($params['limit'],$params['start']);
                # code...
            }else{
                $this->db->limit($params['limit']);
            }
            # code...
        }
        if (isset($params['order_by'])) {
            $this->db->order_by($params['order_by']['name'],$params['order_by']['value']);
        }
        $query = $this->db->get();
        return $query;
     }
	 public function _get_datatables_query($field,$params)
	 {
	 	$select = $params['select'];
	 	$this->db->select($select);
	 	$this->db->from($this->table['name']);
 		if (isset($params['join'])) {
 			# code...
 			foreach ($params['join'] as $key => $value) {
 				# code...
 				// print_r($params['join']);die;
 				$this->db->join($value['table'],$value['id'].'='.$value['as'],$value['join']);
 			}
 		}
        $i = 0;
     	$column_order[] = null;
        foreach ($field as $item) // loop column 
        {
        	$column_order[]= $item['name'];

            if($_GET['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item['name'],$_GET['search']['value']);
                }
                else
                {
                    $this->db->or_like($item['name'],$_GET['search']['value']);
                }
 
                if(count($field) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_GET['order'])) // here order processing
        {
        	foreach ($field as $key => $value) {
        		# code...
        		$column[] = $value['name'];
        	}

            $this->db->order_by($column[$_GET['order']['0']['column']],$_GET['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = array($this->table['id'] => 'DESC');
            $this->db->order_by(key($order), $order[key($order)]);
        }
	 }
	 public function get_datatable($field,$params=array())
	 {
	 	// print_r($params);die;
	 	$this->_get_datatables_query($field,$params);
	 	if($_GET['length'] != -1){
        	$this->db->limit($_GET['length'], $_GET['start']);
    	}
        $query = $this->db->get();
        return $query->result();
	 }
	function count_filtered($params)
    {
    	$field = $this->table['field'];
        $this->_get_datatables_query($field,$params);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table['name']);
        return $this->db->count_all_results();
    }
	 public function insert($data)
	 {
	 	$this->db->insert($this->table['name'],$data);
	 	return $this->db->insert_id();
	 }
	 public function insert_batch($data)
	 {
	 	$this->db->insert_batch($this->table['name'],$data);
	 	return true;
	 }
	 public function update($id,$data)
	 {
	 	$this->db->where($this->table['primary_key'],$id);
	 	$this->db->update($this->table['name'],$data);
	 	return true;
	 }
     public function delete($id)
     {
        $this->db->where($this->table['primary_key'],$id);
        $this->db->delete($this->table['name']);
        return true;
     }
     public function edit($id)
     {
        $this->db->where($this->table['primary_key'],$id);
        return $this->db->get($this->table['name'])->row_array();
     }
}
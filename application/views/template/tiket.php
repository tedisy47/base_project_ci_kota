<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title><?=$title?> - Admin Eterna</title>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
        <link href="https://cdn.jsdelivr.net/npm/litepicker/dist/css/litepicker.css" rel="stylesheet" />
        <link href="<?=base_url()?>assets/css/styles.css" rel="stylesheet" />
        <link rel="icon" type="image/x-icon" href="<?=base_url()?>assets/img/favicon.png" />
        <script data-search-pseudo-elements defer src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/tiket.css">
    </head>
    <body class="nav-fixed">
        <?php $this->load->view('template/partial/menu_top')?>
        <div id="layoutSidenav">
            <?php $this->load->view('template/partial/menu_sidebar')?>
            <div id="layoutSidenav_content">
                <main>
                    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
                        <div class="container-xl px-4">
                            <div class="page-header-content pt-4">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col-auto mt-4">
                                        <h1 class="page-header-title">
                                            <div class="page-header-icon"><i data-feather="activity"></i></div>
                                            <?=$title?>
                                        </h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>
                    <!-- Main page content-->
                    <div class="container-xl px-4 mt-n10">
                        <div class="card mb-4">
                            <div class="card-header">
                                <h5>Detail <?=$title?></h5>       
                            </div>
                            <div class="card-body">
                                <input type="hidden" id="base_url" value="<?=base_url()?>">
                                <?php foreach ($tiket_detail as $key => $value): ?>
                                <div class="comment">
                                    <div class="comment-author-ava"><img src="https://bootdey.com/img/Content/avatar/avatar6.png" alt="Avatar"></div>
                                    <div class="comment-body">
                                        <p class="comment-text"><?=$value['comment']?></p>
                                        <div class="comment-footer"><span class="comment-meta"><?=$value['name']?></span></div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                                <!-- Reply Form-->
                                <h5 class="mb-30 padding-top-1x">Leave Message</h5>
                                <form method="post" action="<?=site_url('Master/tiket/submit_comment')?>">
                                    <div class="form-group mb-3">
                                        <input type="hidden" name="tiket_id" value="<?=$tiket_id?>">
                                        <textarea name="comment" class="form-control form-control-rounded" id="review_text" rows="8" placeholder="Write your message here..." required=""></textarea>
                                    </div>
                                    <div class="text-right">
                                        <button class="btn btn-outline-primary" type="submit">Submit Message</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                </main>
                <footer class="footer-admin mt-auto footer-light">
                    <div class="container-xl px-4">
                        <div class="row">
                            <div class="col-md-6 small">Copyright &copy; Your Website 2021</div>
                            <div class="col-md-6 text-md-end small">
                                <a href="#!">Privacy Policy</a>
                                &middot;
                                <a href="#!">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?=base_url()?>assets/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js" crossorigin="anonymous"></script>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/litepicker/dist/bundle.js" crossorigin="anonymous"></script>
        <script src="<?=base_url()?>assets/js/litepicker.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <?= $this->session->flashdata('alert')?>
        <?php if (isset($js)):?>
            <script type="text/javascript" src="<?=base_url($js)?>"></script>
        <?php endif; ?>
    </body>
</html>

import cv2 ### Load opencv
image = cv2.imread('your_image_path') ### returns a NumPy array representing the image.
cv2.imshow("test", image) ### display the image in a window
cv2.waitKey(0) ### pauses the execution of the script until we press a key on our keyboard
cv2.destroyAllWindows()
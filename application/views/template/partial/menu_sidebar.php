<div id="layoutSidenav_nav">
                <nav class="sidenav shadow-right sidenav-light">
                    <div class="sidenav-menu">
                        <div class="nav accordion" id="accordionSidenav">
                            <?php if ($this->session->userdata('user_id')): ?>
                            <!-- Sidenav Menu Heading (Account)-->
                                <a class="nav-link collapsed" href="<?=site_url()?>">
                                    <div class="nav-link-icon"><i data-feather="activity"></i></div>
                                    Dashboards
                                </a>
                                <!-- Sidenav Heading (Custom)-->
                                <?php if ($this->session->userdata('hak_akses')==1): ?>
                                    <div class="sidenav-menu-heading">Master</div>
                                    <!-- Sidenav Accordion (Pages)-->
                                    <a class="nav-link collapsed" href="<?=site_url('master/Account_ig')?>">
                                        <div class="nav-link-icon"><i data-feather="grid"></i></div>
                                        Account IG
                                    </a>
                                    <a class="nav-link collapsed" href="<?=site_url('master/Account_twitter')?>">
                                        <div class="nav-link-icon"><i data-feather="grid"></i></div>
                                        Account Twitter
                                    </a>
                                    <a class="nav-link collapsed" href="<?=site_url('master/price')?>">
                                        <div class="nav-link-icon"><i data-feather="grid"></i></div>
                                        Price
                                    </a>
                                    <a class="nav-link collapsed" href="<?=site_url('master/Tiket')?>">
                                        <div class="nav-link-icon"><i data-feather="grid"></i></div>
                                        Tiket
                                    </a>
                                <?php endif; ?>
                                 <!--  start order -->
                                <div class="sidenav-menu-heading">Order</div>
                                <a class="nav-link collapsed" href="<?=site_url('order/instagram/form')?>">
                                    <div class="nav-link-icon"><i data-feather="grid"></i></div>
                                    Instagram
                                </a>
                                <!-- end order -->
                                <!-- start history -->
                                <div class="sidenav-menu-heading">History</div>
                                <a class="nav-link collapsed" href="<?=site_url('order/instagram/')?>">
                                    <div class="nav-link-icon"><i data-feather="grid"></i></div>
                                    Instagram
                                </a>
                                <!-- end history -->
                            <?php endif; ?> 
                        </div>
                    </div>
                    <!-- Sidenav Footer-->
                    <div class="sidenav-footer">
                        <div class="sidenav-footer-content">
                            <div class="sidenav-footer-subtitle">Logged in as:</div>
                            <div class="sidenav-footer-title"><?=$this->session->userdata('name')?></div>
                        </div>
                    </div>
                </nav>
            </div>

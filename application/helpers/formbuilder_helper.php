<?php

function form_bulder($field,$option=array(),$data=array())
{
	// echo "<pre>";
	// print_r($option);die;
	$ci = &get_instance();

      //load the session library
      $ci->load->library('session');

	$form = null;
	foreach ($field as $field_key => $field_value) {
		if($field_value['form_show']=='Y') {

			// start values
			if (!empty($data[$field_value['name']])) {
				$input_value[$field_value['name']] = $data[$field_value['name']];
			}else if (!empty($ci->session->flashdata('data_'.$field_value['name']))) {
				$input_value[$field_value['name']] = $ci->session->flashdata('data_'.$field_value['name']);
			}else{
				$input_value[$field_value['name']] = '';
			}
			// end values
			// start build form
			switch ($field_value['type']) {
				case 'HIDDEN':
					$form .= '<input type="hidden" name="'.$field_value['name'].'" value="'.$input_value[$field_value['name']].'" class="'.$field_value['class'].'" id="'.$field_value['id'].'">';
					break;
				case 'NUMBER':
					$form .= '<div class="'.$field_value['col'].'">';
					$form .= '<div class="form-group">';
					$form .= '<label>'.$field_value['label'].'<i class="text-danger">'.$ci->session->flashdata('error_'.$field_value['name']).'</i></label>';
					$form .= '<input type="number" name="'.$field_value['name'].'" value="'.$input_value[$field_value['name']].'" class="'.$field_value['class'].'" id="'.$field_value['id'].'">';
					$form .= '</div>';
					$form .= '</div>';
					break;
				case 'FILE':
					$form .= '<div class="'.$field_value['col'].'">';
					$form .= '<div class="form-group">';
					$form .= '<label>'.$field_value['label'].'<i class="text-danger">'.$ci->session->flashdata('error_'.$field_value['name']).'</i></label>';
					$form .= '<input type="file" name="'.$field_value['name'].'" class="'.$field_value['class'].'" id="'.$field_value['id'].'">';
					$form .= '</div>';
					$form .= '</div>';
					break;
				case 'TEXTAREA':
					$form .= '<div class="'.$field_value['col'].'">';
					$form .= '<div class="form-group">';
					$form .= '<label>'.$field_value['label'].'<i class="text-danger">'.$ci->session->flashdata('error_'.$field_value['name']).'</i></label>';
					$form .= '<textarea name="'.$field_value['name'].'" class="'.$field_value['class'].'" id="'.$field_value['id'].'">'.$input_value[$field_value['name']].'</textarea>';
					$form .= '</div>';
					$form .= '</div>';
					break;
				case 'select':
					$form .= '<div class="'.$field_value['col'].'">';
					$form .= '<div class="form-group">';
					$form .= '<label>'.$field_value['label'].'<i class="text-danger">'.$ci->session->flashdata('error_'.$field_value['name']).'</i></label>';
					$form .= '<select name="'.$field_value['name'].'" class="'.$field_value['class'].'" id="'.$field_value['id'].'">';
					// buid options
					$form .= '<option value=""> Pilih '.$field_value['label'].'</option>';
					foreach ($option[$field_value['name']] as $key_option => $value_option) {
						# code...
						$form .= '<option value="'.$value_option['value'].'">'.$value_option['label'].'</option>';
					}
					$form .= '</select>';
					$form .= '</div>';
					$form .= '</div>';
					break;
				case 'RADIO':
					$form .= '<div class="'.$field_value['col'].'">';
					$form .= '<div class="form-group">';
					$form .= '<label>'.$field_value['label'].'<i class="text-danger">'.$ci->session->flashdata('error_'.$field_value['name']).'</i></label><br>';
					// buid options
					foreach ($option[$field_value['name']] as $key_option => $value_option) {
						# code...
						$form .= '<input type="radio" name="'.$field_value['name'].'" values="'.$value_option['value'].'">'.$value_option['label'];
					}
					$form .= '</div>';
					$form .= '</div>';
					break;
				
				default:
					$form .= '<div class="'.$field_value['col'].'">';
					$form .= '<div class="form-group">';
					$form .= '<label>'.$field_value['label'].'<i class="text-danger">'.$ci->session->flashdata('error_'.$field_value['name']).'</i></label>';
					$form .= '<input type="text" name="'.$field_value['name'].'" value="'.$input_value[$field_value['name']].'" class="'.$field_value['class'].'" id="'.$field_value['id'].'">';
					$form .= '</div>';
					$form .= '</div>';
					break;
			}
			// end start build form
		}
	}
	return $form;
	// echo "<pre>";
	// print_r($field);die;
}